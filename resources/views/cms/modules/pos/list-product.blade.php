@if(!empty($products) && count($products) > 0)
    <ul style="margin: 0;padding: 5px;border: 1px solid #ccc;border-top: none;">
        @foreach($products as $product)
            <li style="list-style: none;" id="product-on-pos" data-id="{{ $product->id }}">
                <a href="javascript:void(0)" style="color: blue;">{{ $product->product_code }} - {{ $product->product_name }} - {{ $product->product_sell_price }}</a>
            </li>
        @endforeach
    </ul>
@endif
