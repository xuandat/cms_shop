@if(!empty($customers) && count($customers) > 0)
    <ul style="margin: 0;padding: 5px;border: 1px solid #ccc;border-top: none;">
        @foreach($customers as $customer)
            <li style="list-style: none;" id="customer-on-pos" data-id="{{ $customer->id }}" data-code="{{ $customer->customer_code }}" data-name="{{ $customer->customer_name }}">
                <a href="javascript:void(0)" style="color: blue;">{{ $customer->customer_code }} - {{ $customer->customer_name }}</a>
            </li>
        @endforeach
    </ul>
@endif
